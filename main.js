function createNewUser() {
  let firstname = prompt("ENter your name pls ->");
  let lastname = prompt("ENter your surname pls ->");
  let userBirthdate = prompt(
    `Enter your birth date in DD.MM.YYYY format pls ->`
  );
  let userBirthDate = new Date();
  userBirthDate.setFullYear(userBirthdate.slice(6));
  userBirthDate.setMonth(userBirthdate.slice(3, 5));
  userBirthDate.setDate(userBirthdate.slice(0, 2));

  let newUser = {
    _firstName: firstname,
    _lastName: lastname,
    _birthDate: userBirthDate,

    getLogin() {
      return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
    },
    getPassword() {
      return (
        this._firstName[0].toUpperCase() +
        this._lastName.toLowerCase() +
        this._birthDate.getFullYear()
      );
    },
    getAge() {
      return Math.floor(
        (Date.now().toString() - this._birthDate.getTime()) / 31536000000
      );
    },

    setFirstName(a) {
      this._firstName = a;
    },
    setLastName(a) {
      this._lastName = a;
    },
  };

  // newUser.setFirstName("Voodoo");

  console.log(newUser.getPassword());
  console.log(newUser.getAge());
}
createNewUser();
